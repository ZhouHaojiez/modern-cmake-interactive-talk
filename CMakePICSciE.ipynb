{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Modern CMake\n",
    "#### Henry Schreiner\n",
    "\n",
    "PICSciE Lunch Talk 2019-8-13\n",
    "\n",
    "<img alt=\"IRIS-HEP\" src=\"iris-hep-logo.png\" style=\"float:right;\"/>\n",
    "\n",
    "Links: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/CLIUtils%2Fmodern-cmake-interactive-talk/master?urlpath=lab/tree/CMakePICSciE.ipynb)\n",
    "* The book: [cliutils.gitlab.io/modern-cmake](https://cliutils.gitlab.io/modern-cmake)\n",
    "* My blog: [iscinumpy.gitlab.io](https://iscinumpy.gitlab.io)\n",
    "* The workshop: [github.com/henryiii/cmake_workshop](https://github.com/henryiii/cmake_workshop)\n",
    "* This talk: [gitlab.com/CLIUtils/modern-cmake-interactive-talk](https://gitlab.com/CLIUtils/modern-cmake-interactive-talk)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Part 1: Intro to CMake"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "cmake --version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# What is CMake?\n",
    "\n",
    "Is it a build system?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Build system example (Rake):\n",
    "\n",
    "```ruby\n",
    "# 01-rake/Rakefile\n",
    "\n",
    "task default: [:hello_world] do                       # hello_world.c \n",
    "     puts 'All built'                                 #      ↓\n",
    "end                                                   # hello_world\n",
    "                                                      #      ↓\n",
    "file hello_world: ['hello_world.c'] do |t|            # default task\n",
    "    sh \"gcc #{t.prerequisites.join(' ')} -o #{t.name}\"\n",
    "end\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "(cd 01-rake && rake)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Features:\n",
    "* Understands **when** to build/rebuild\n",
    "* Doesn't understand **how** to build\n",
    "* Generic; can be used for anything\n",
    "\n",
    "### Examples\n",
    "* `make`: Classic, custom syntax\n",
    "* `rake`: Ruby make\n",
    "* `ninja`: Google's entry, not designed to be hand written"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Build system generator\n",
    "\n",
    "* Understands the files you are building\n",
    "* System independent\n",
    "* You give relationships\n",
    "* Can find libraries, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Build system generator example (CMake):"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```cmake\n",
    "# 01-rake/CMakeLists.txt\n",
    "cmake_minimum_required(VERSION 3.11)\n",
    "\n",
    "project(HelloWorld)\n",
    "\n",
    "add_executable(hello_world hello_world.c)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "cmake -S 01-rake -B 01-build\n",
    "cmake --build 01-build"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### C/C++ Examples\n",
    "* `cmake`: Cross-platform Make (also Fortran, CUDA, C#, Swift)\n",
    "* `scons`: Software Carpentry Construction (Python)\n",
    "* `meson`: Newer Python entry\n",
    "* `basel`: Google's build system\n",
    "* Other languages often have *their own build system* (Rust, Go, Python, ...)\n",
    "\n",
    "> We will follow common convention and call these \"build-systems\" for short from now on"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Why CMake?\n",
    "\n",
    "![Interest over time](IntrestOverTime.pdf)\n",
    "\n",
    "\n",
    "It has become a standard\n",
    "\n",
    "* Approximately all IDEs support it\n",
    "* Many libraries have built-in support\n",
    "* Integrates with almost everything"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Custom Buildsystems are going away\n",
    "\n",
    "* [Qt 6 is dropping QMake](https://blog.qt.io/blog/2019/08/07/technical-vision-qt-6/) for CMake (note: C++17 too)\n",
    "* Boost is starting to support CMake at a reasonable level along with BJam\n",
    "* Standout: Google is dual supporting Basel and CMake"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# (More) Modern CMake\n",
    "\n",
    "* CMake is a language to learn (at is a bit odd)\n",
    "* Classic CMake was ugly and had problems - but it doesn't have to be anymore!\n",
    "\n",
    "* *Modern CMake* and [*More Modern CMake*](https://github.com/Bagira80/More-Modern-CMake/blob/master/MoreModernCMake.pdf)!\n",
    "    * CMake 3.0 in 2014: Modern CMake begins\n",
    "    * CMake 3.1-3.4 had important additions/fixes\n",
    "    * CMake 3.12 in mid 2018 completed the \"More Modern CMake\" phase\n",
    "    * Current CMake is 3.15\n",
    "* Eras of CMake\n",
    "    * Classic CMake: Directory based\n",
    "    * Modern CMake: Target based\n",
    "    * More Modern CMake: Unified target behavior"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Minimum Version\n",
    "\n",
    "CMake has a (AFAIK) unique version system.\n",
    "\n",
    "If a file start with this:\n",
    "\n",
    "```cmake\n",
    "cmake_minimum_required(VERSION 3.0)\n",
    "```\n",
    "\n",
    "Then CMake will set all *policies* (which cover all *behavior* changes) to their 3.0 settings. This in theory means it is extremely backward compatible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "You can also do this:\n",
    "\n",
    "```cmake\n",
    "cmake_minimum_required(VERSION 3.4...3.14)\n",
    "```\n",
    "\n",
    "Then\n",
    "* CMake < 3.4 will be an error\n",
    "* CMake 3.4 -- 3.11 will set 3.4 policies (feature was introduced in 3.12, but syntax is valid)\n",
    "* CMake 3.12 -- 3.14 will set current policies\n",
    "* CMake 3.15+ will set 3.14 policies"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "### Rule:\n",
    "\n",
    "* Set the highest minimum you can (build systems are hard/ugly enough as it is)\n",
    "* Test with the current version and set that as the upper limit if it works"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Installing CMake\n",
    "\n",
    "Built-in versions can be old, but CMake is *very* easy to install!\n",
    "\n",
    "* Portable builds for major platforms\n",
    "* One line Docker install\n",
    "* Available on PyPI, Conda, Homebrew, Chocolaty, and more\n",
    "\n",
    "See [Modern CMake: Installing](https://cliutils.gitlab.io/modern-cmake/chapters/intro/installing.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Running CMake\n",
    "\n",
    "The classic method:\n",
    "```cmake\n",
    "mkdir build\n",
    "cd build\n",
    "cmake ..\n",
    "make\n",
    "```\n",
    "\n",
    "\n",
    "The modern method:\n",
    "```cmake\n",
    "cmake -S . -B build\n",
    "cmake --build build\n",
    "```\n",
    "\n",
    "(Now) supports `-v` (verbose), `-j N` (threads), `-t target`, and more"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Example options:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cmake --build 01-build -v"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Projects\n",
    "\n",
    "The second required line for a project.\n",
    "\n",
    "```cmake\n",
    "project(MyProject\n",
    "  VERSION                                 # 3.0\n",
    "    1.2.3\n",
    "  DESCRIPTION                             # 3.9\n",
    "    \"I am a description\"\n",
    "  LANGUAGES\n",
    "    CXX C CUDA Fortran Swift CSharp ASM\n",
    "  HOMEPAGE_URL                            # 3.12\n",
    "    https://google.com\n",
    ")\n",
    "```\n",
    "\n",
    "* Fields are optional, but can be used by CPack, Doxygen, and in other places"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## CMake Syntax\n",
    "\n",
    "* Quotes are optional, but spaces split into array\n",
    "* Case is upper or lower for functions/macros\n",
    "* Case does matter for variables\n",
    "* Variables are `${NAME}` and can be nested\n",
    "* Most functions/macros have KEYWORDS in allcaps\n",
    "* Setting/accessing variables and properties is not part of the language"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Targets\n",
    "\n",
    "Excutables and libraries are *targets*\n",
    "\n",
    "<img src=\"04-mermaid-libs.svg\" alt=\"Library dependency example\" width=\"500\" style=\"float:right;\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "* Targets have private and interface properties\n",
    "* Targets can depend on each other `PRIVATE`ly or `PUBLIC`ally, or as an `INTERFACE`\n",
    "* An `IMPORTED` target is built by someone else\n",
    "* An `INTERFACE` target is not built\n",
    "\n",
    "\n",
    "```cmake\n",
    "add_library(MyLibrary mylibrary.cpp mylibrary.h)\n",
    "target_link_libraries(MyLibrary PRIVATE OpenMP::OpenMP_CXX) # OpenMP CMake 3.9+\n",
    "```\n",
    "\n",
    "Properties include:\n",
    "\n",
    "* Header include directories\n",
    "* Compile flags and definitions\n",
    "* Link flags\n",
    "* C++ standard and/or compiler features required\n",
    "* Linked libraries\n",
    "\n",
    "Note that other things include properties, like files (such as LANGUAGE), directories, and global"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Variables and the Cache\n",
    "\n",
    "CMake has a variable system:\n",
    "\n",
    "```cmake\n",
    "set(ITEM \"HI\")\n",
    "messsage(STATUS \"${ITEM}\")\n",
    "```\n",
    "\n",
    "Like Bash, accessing an uninitialized variable is not an error. Variables can be nested.\n",
    "\n",
    "CMake also has a caching system; see `CMakeCache.txt` in the build dir.\n",
    "\n",
    "```cmake\n",
    "set(ITEM \"HI\" CACHE STRING \"Description\")\n",
    "```\n",
    "\n",
    "This will set it the first time it is cached, then will read from the cache from then on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Property initializers\n",
    "\n",
    "A convention (and much of CMake runs on convention) is:\n",
    "\n",
    "For a property named `MY_PROPERTY`, the default comes from a variable `CMAKE_MY_PROPERTY`.\n",
    "\n",
    "So you can set a variable to keep your code DRY."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# The CMake language:\n",
    "\n",
    "* Commands (Functions and Macros)\n",
    "* Targets\n",
    "* Variables\n",
    "* Properties\n",
    "* Generator expressions: Generation time code\n",
    "* Modules: add functionality\n",
    "* Find/Configure packages: CMake, library, or user provided"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Part 2: What you might not know"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Downloading dependencies\n",
    "\n",
    "CMake can download your dependencies for you, and can integrate with files\n",
    "\n",
    "* Composable (sub-)projects: One project can include another\n",
    "    * Does not have namespaces, can cause target collisions\n",
    "* Build time data and project downloads: `ExternalProject` (classic)\n",
    "* Configure time downloads `FetchContent` (new in 3.11+)\n",
    "* You can also use submodules (my favorite method)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "`02-fetch/hello_fmt.cpp`\n",
    "\n",
    "```c++\n",
    "#include <fmt/format.h>\n",
    "\n",
    "int main() {\n",
    "    fmt::print(\"The answer is {}\\n\", 42);\n",
    "    return 0;\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "`02-fetch/CMakeLists.txt`\n",
    "\n",
    "```cmake\n",
    "cmake_minimum_required(VERSION 3.14)\n",
    "project(HelloWorld LANGUAGES CXX)\n",
    "\n",
    "include(FetchContent)\n",
    "FetchContent_Declare(\n",
    "  fmt\n",
    "  GIT_REPOSITORY https://github.com/fmtlib/fmt.git\n",
    "  GIT_TAG        5.3.0)\n",
    "FetchContent_MakeAvailable(fmt)\n",
    "\n",
    "add_executable(hello_world hello_fmt.cpp)\n",
    "target_link_libraries(hello_world PRIVATE fmt::fmt)\n",
    "target_compile_features(hello_world PRIVATE cxx_std_11)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "cmake -S 02-fetch -B 02-build\n",
    "cmake --build 02-build"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Imported targets\n",
    "* Now (3.11+) can be built with standard CMake commands!\n",
    "* Can now be global with `IMPORTED_GLOBAL`\n",
    "\n",
    "```cmake\n",
    "add_library(ExternLib IMPORTED INTERFACE)\n",
    "\n",
    "# Classic                           # Modern\n",
    "set_property(                       target_include_directories(\n",
    "    TARGET                              ExternLib INTERFACE /my/inc\n",
    "      ExternLib                     )\n",
    "    APPEND\n",
    "    PROPERTY\n",
    "      INTERFACE_INCLUDE_DIRECTORIES\n",
    "        /my/inc\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# CUDA as a language\n",
    "\n",
    "Cuda is now a first-class language in CMake! (3.9+) Replaces FindCuda.\n",
    "\n",
    "```cmake\n",
    "project(MY_PROJECT LANGUAGES CUDA CXX)\n",
    "\n",
    "# Or for optional CUDA support\n",
    "project(MY_PROJECT LANGUAGES CXX)\n",
    "include(CheckLanguage)\n",
    "check_language(CUDA)\n",
    "if(CMAKE_CUDA_COMPILER)\n",
    "    enable_language(CUDA)\n",
    "endif()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Much like you can set C++ standards, you can set CUDA standards too:\n",
    "\n",
    "```cmake\n",
    "if(NOT DEFINED CMAKE_CUDA_STANDARD)\n",
    "    set(CMAKE_CUDA_STANDARD 11)\n",
    "    set(CMAKE_CUDA_STANDARD_REQUIRED ON)\n",
    "endif()\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "You can add files with `.cu` extensions and they compile with nvcc. (You can always set the LANGUAGE property on a file, too). Separable compilation is a property:\n",
    "\n",
    "```cmake\n",
    "set_target_properties(mylib PROPERTIES\n",
    "                            CUDA_SEPERABLE_COMPILATION ON)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Extra tools\n",
    "\n",
    "Useful properties:\n",
    "* `INTERPROCEDURAL_OPTIMIZATION`: Add IPO\n",
    "* `POSITION_INDEPENDENT_CODE`: Add `-fPIC`\n",
    "* `<LANG>_COMPILER_LAUNCHER`: Add `ccache`\n",
    "* `<LANG>_CLANG_TIDY`\n",
    "* `<LANG>_CPPCHECK`\n",
    "* `<LANG>_CPPLINT`\n",
    "* `<LANG>_INCLUDE_WHAT_YOU_USE`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Useful modules:\n",
    "\n",
    "* `CheckIPOSupported`: See if IPO is supported by your compiler\n",
    "* `CMakeDependentOption`: Make one option depend on another\n",
    "* `CMakePrintHelpers`: Handy debug printing\n",
    "* `FeatureSummary`: Record or printout enabled features and found packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# CompilerDetection and Flag checking\n",
    "\n",
    "`try_compile` / `try_run` can tell you if a flag or file works.  However, first check:\n",
    "\n",
    "* `CheckCXXCompilerFlag`\n",
    "* `CheckIncludeFileCXX`\n",
    "* `CheckStructHasMember`\n",
    "* `TestBigEndian`\n",
    "* `CheckTypeSize`\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Even better: `WriteCompilerDetectionHeader` will write out C/C++ macros for your compiler for you!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "`03-compiler/CMakeLists.txt`\n",
    "\n",
    "```cmake\n",
    "cmake_minimum_required(VERSION 3.15)\n",
    "project(CompilerExample LANGUAGES CXX)\n",
    "\n",
    "include(WriteCompilerDetectionHeader)\n",
    "\n",
    "write_compiler_detection_header(\n",
    "  FILE my_compiler_detection.h\n",
    "  PREFIX MyPrefix\n",
    "  COMPILERS\n",
    "    GNU Clang MSVC Intel\n",
    "  FEATURES\n",
    "    cxx_variadic_templates\n",
    "    cxx_nullptr\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "cmake -S 03-compiler -B 03-build"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
